package por.layout;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriverException;

import por.core.Driver;

public class Options {
    public static By REF = By.xpath("//ion-popover");

    public static By optionItm(String optionName) {
        //return By.xpath("//android.view.View[contains(@content-desc,'"+optionName+"')]");
        return By.xpath(".//ion-item[starts-with(normalize-space(),'"+optionName+"')]");
    }

    public static final class OPTIONS {
        public static final String USING_API = "Using API";
        public static final String USING_MOCK = "Using Mock";
        public static final String NOTIFICATIONS = "Notifications";
    }

    public static void select(Driver driver, String optionName) {
        driver.get(optionItm(optionName),REF).click();
        driver.sleep(Driver.INTERACTION_TIMEOUT);
    }
    public static void takeSnap(Driver driver) {
int i=1 ;
		
		try {
			FileUtils.copyFile(((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE),
					new File("./reports/images/" +"SS"+  + i +".jpg"));
			  i++;
		} catch (WebDriverException e) {

		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		
	} 
  
}
